function validatingName(name) {
    if (name.trim() === '') {
        return "Name can't be empty";
    } else if (/\d/.test(name) === true) {
        return `Name should not contain any digit`;
    } else if (/[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(name)) {
        return "Name shouldn't contain any special chars";
    } else if (name.trim().length < 3 || name.trim().length >20) {
        return "Name length should be between 3 - 20 chars";
    } else {
        return "";
    }
}

function validatingEmail(email){
    if(email.trim() === ''){
        return "Email can't be empty";
    }if(email.trim().length > 50){
        return "Email length should be between 50 chars";
    }else if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email) === true){
        return "";
    }else{
        return "Invalid email";
    }
}

function validatingPassword(password) {
    if (password.trim() === '') {
        return "Password can't be empty";
    } else if (password.trim().length < 8 || password.trim().length >16) {
        return "Password length should be between 8-16 chars";
    } else if ((password.search(/[a-z]/i) < 0) || password.search(/[0-9]/) < 0) {
        return "Password should contain atleast 1 alhpabet and digit";
    } else {
        return "";
    }
}

function matchingPassword(password, confirmPassword) {
    if (password !== confirmPassword) {
        return "Password doesn't match";
    }else{
        return "";
    }
}

document.getElementById('form').addEventListener('submit', (event) => {

    event.preventDefault();

    const firstName = document.getElementById('firstName').value;
    const lastName = document.getElementById('lastName').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const confirmPassword = document.getElementById('confirmPassword').value;
    const tos = document.getElementById('tos').value;

    const firstNameErrorDiv = document.getElementById('firstNameError');
    firstNameErrorDiv.innerText = validatingName(firstName);

    const lastNameErrorDiv = document.getElementById('lastNameError');    
    lastNameErrorDiv.innerText = validatingName(lastName);

    const emailErrorDiv = document.getElementById('emailError');
    emailErrorDiv.innerText = validatingEmail(email);

    const passwordErrorDiv = document.getElementById('passwordError');
    passwordErrorDiv.innerText = validatingPassword(password);

    const confirmPasswordErrorDiv = document.getElementById('confirmPasswordError');
    confirmPasswordErrorDiv.innerText = matchingPassword(password,confirmPassword);

    if(validatingName(firstName) === '' && validatingName(lastName) === '' && validatingEmail(email) ==='' && validatingPassword(password) === '' && matchingPassword(password,confirmPassword)=== ''){

        const user = {
            firstName,
            lastName,
            email
        }

        localStorage.setItem("user",JSON.stringify(user));
        location.href ='index.html';
    }else{
        return;
    }
});

document.getElementById('logout').addEventListener('click',()=>{
    localStorage.clear();
    location.reload();
});