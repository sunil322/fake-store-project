fetch('https://fakestoreapi.com/products')
    .then((response) => {
        if (response.ok) {
            return response.json();
        }
    })
    .then((data) => {
        
        document.querySelector('.lds-ring').remove();
        
        const allProductsDiv = data.map((productData) => {

            let productDiv = document.createElement('div');
            productDiv.className="product";

            let img = document.createElement('img');
            img.src = productData.image;
            img.alt = "product-img";

            let category = document.createElement('p');
            category.className = "category";
            category.innerText = productData.category;

            let title = document.createElement('h4');
            title.innerText = productData.title;
            title.className='title';
            
            let descriptionDiv = document.createElement('div');
            let description = document.createElement('p');
            descriptionDiv.className= "description-div";
            description.innerText = productData.description;
            descriptionDiv.appendChild(description);

            let price = document.createElement('p');
            price.innerHTML = `$ ${productData.price}`;
            price.className='price';

            let rating = document.createElement('p');
            rating.className="rating";
            rating.innerHTML = `<i class="fa-solid fa-star"></i> ${productData.rating.rate} (${productData.rating.count})`;

            let button = document.createElement('button');
            button.className = 'button';
            button.innerHTML = 'BUY NOW';

            productDiv.appendChild(img);
            productDiv.appendChild(category);
            productDiv.appendChild(title);
            productDiv.appendChild(descriptionDiv);
            productDiv.appendChild(price);
            productDiv.appendChild(rating);
            productDiv.appendChild(button);
        
            return productDiv;
        });
        document.getElementById('products-container').append(...allProductsDiv);
    })
    .catch((err) => {
        
        document.querySelector('.lds-ring').remove();
        
        let errorDiv = document.createElement('div');
        errorDiv.className = "error";
        let h1 = document.createElement('h1'); 
        h1.innerText = 'Error in loading products';
        errorDiv.appendChild(h1);
        document.getElementById('products-container').appendChild(errorDiv);
    });

    document.getElementById('logout').addEventListener('click',()=>{
        localStorage.clear();
        location.reload();
    });